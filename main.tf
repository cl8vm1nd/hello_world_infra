terraform {
  required_version = "0.11.11"

  backend "gcs" {
    bucket = "terraform-backend-my-tech"
    prefix = "terraform/state"
  }
}

########
# Provider Block
########

provider "google" {
  credentials = "${file("${var.provider_credentials_key}")}"
  project     = "${var.GC_provider["project"]}"
  region      = "${var.GC_provider["region"]}"
}

provider "helm" {
  install_tiller = true

  kubernetes {
    host     = "${google_container_cluster.hello_world_cluster.endpoint}"
    username = "${var.cluster_master_auth["name"]}"
    password = "${var.cluster_master_auth["password"]}"

    client_certificate     = "${file("~/.kube/keys/client-cert.pem")}"
    client_key             = "${file("~/.kube/keys/client-key.pem")}"
    cluster_ca_certificate = "${file("~/.kube/keys/cluster-ca-cert.pem")}"
  }
}

provider "kubernetes" {
  host     = "${google_container_cluster.hello_world_cluster.endpoint}"
  username = "${var.cluster_master_auth["name"]}"
  password = "${var.cluster_master_auth["password"]}"

  client_certificate     = "${file("~/.kube/keys/client-cert.pem")}"
  client_key             = "${file("~/.kube/keys/client-key.pem")}"
  cluster_ca_certificate = "${file("~/.kube/keys/cluster-ca-cert.pem")}"
}

########
# Cluster Block
########

resource "google_container_node_pool" "hello-world-cluster-pool" {
  name               = "${var.cluster_node_pool["name"]}"
  region             = "${var.cluster_node_pool["region"]}"
  cluster            = "${google_container_cluster.hello_world_cluster.name}"
  initial_node_count = "${var.cluster_node_pool["initial_node_count"]}"

  autoscaling {
    max_node_count = "${var.cluster_node_pool["autoscaling_max_node"]}"
    min_node_count = "${var.cluster_node_pool["autoscaling_min_node"]}"
  }

  management {
    auto_repair  = "${var.cluster_node_pool["auto_repair"]}"
    auto_upgrade = "${var.cluster_node_pool["auto_upgrade"]}"
  }

  lifecycle {
    create_before_destroy = true
  }

  node_config {
    disk_size_gb = "${var.cluster_node_pool["disk_size_gb"]}"
    machine_type = "${var.cluster_node_pool["machine_type"]}"
    disk_type    = "${var.cluster_node_pool["disk_type"]}"
    oauth_scopes = "${var.cluster_node_pool_oauth}"
  }
}

resource "google_container_cluster" "hello_world_cluster" {
  name = "${lookup(var.cluster_data_basic, "name")}"

  initial_node_count       = "${lookup(var.cluster_nodes, "node_count")}"
  enable_legacy_abac       = "${lookup(var.cluster_nodes, "abac")}"
  region                   = "${lookup(var.cluster_data_basic, "region")}"
  additional_zones         = "${var.cluster_additional_zones}"
  min_master_version       = "${var.cluster_min_master_version}"
  remove_default_node_pool = "${var.cluster_data_basic["remove_default_node_pool"]}"

  addons_config {
    http_load_balancing {
      disabled = "${var.cluster_data_basic["http_load_balancing_off"]}"
    }

    horizontal_pod_autoscaling {
      disabled = "${var.cluster_data_basic["horizontal_pod_autoscaling_off"]}"
    }

    kubernetes_dashboard {
      disabled = "${var.cluster_data_basic["kubernetes_dashboard_off"]}"
    }
  }

  lifecycle {
    create_before_destroy = true

    #prevent_destroy       = true
    ignore_changes = ["node_pool"]
  }

  master_auth {
    username = "${var.cluster_master_auth["name"]}"
    password = "${var.cluster_master_auth["password"]}"
  }
}

########
# Backend Block
########

resource "google_storage_bucket" "terraform-backend" {
  name     = "${lookup(var.terraform_backend_bucket, "name")}"
  location = "${lookup(var.terraform_backend_bucket, "location")}"
  project  = "${lookup(var.GC_provider, "project")}"

  versioning = {
    enabled = "${var.terraform_backend_bucket["versioning"]}"
  }
}

########
# IngressIP Block
########

resource "google_compute_address" "ingress-static" {
  name         = "ingress-static"
  network_tier = "PREMIUM"

  lifecycle {
    prevent_destroy = true
  }
}

########
# Ingress Chart Block
########

resource "helm_release" "nginx-ingress" {
  name      = "${var.ingress-chart["name"]}"
  chart     = "${var.ingress-chart["chart"]}"
  namespace = "${var.ingress-chart["namespace"]}"

  lifecycle {
    create_before_destroy = true
  }

  set {
    name  = "controller.service.loadBalancerIP"
    value = "${google_compute_address.ingress-static.address}"
  }

  set {
    name  = "controller.replicaCount"
    value = "${var.ingress-chart["replicaCount"]}"
  }

  set {
    name  = "controller.updateStrategy.type"
    value = "${var.ingress-chart["updateStrategyType"]}"
  }

  set {
    name  = "rbac.create"
    value = "${var.ingress-chart["rbac"]}"
  }
}

########
# Jenkins Chart Block
########

resource "helm_release" "jenkins" {
  name      = "${var.jenkins-chart["name"]}"
  chart     = "${var.jenkins-chart["chart"]}"
  namespace = "${var.jenkins-chart["namespace"]}"

  lifecycle {
    create_before_destroy = true
  }

  set {
    name  = "AdminPassword"
    value = "${var.jenkins_admin_password}"
  }

  values = [
    "${file("jenkins_values.yaml")}",
  ]
}

########
# Namespaces Block
########

resource "kubernetes_namespace" "production" {
  metadata {
    annotations {
      name = "${var.prod-namespace["name"]}"
    }

    name = "${var.prod-namespace["name"]}"
  }
}

########
# Outputs Block
########

output "client_certificate" {
  value = "${google_container_cluster.hello_world_cluster.master_auth.0.client_certificate}"
}

output "client_key" {
  value = "${google_container_cluster.hello_world_cluster.master_auth.0.client_key}"
}

locals {
  kubeconfig = <<KUBECONFIG


apiVersion: v1
clusters:
- cluster:
    server: https://${google_container_cluster.hello_world_cluster.endpoint}
    certificate-authority-data: ${google_container_cluster.hello_world_cluster.master_auth.0.cluster_ca_certificate}
  name: kubernetes
contexts:
- context:
    cluster: kubernetes
    user: developer
  name: dev
current-context: dev
kind: Config
preferences: {}
users:
- name: developer
  user:
    password: ${var.cluster_master_auth["password"]}
    username: ${var.cluster_master_auth["name"]}
KUBECONFIG
}

output "kubeconfig" {
  value = "${local.kubeconfig}"
}

output "backend_bucket_url" {
  value = "${google_storage_bucket.terraform-backend.url}"
}

output "ingress-static-ip" {
  value = "${google_compute_address.ingress-static.address}"
}
