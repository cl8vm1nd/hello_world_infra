########
# Sensitive variables
########

variable "provider_credentials_key" {
  description = "Main key for provider connection"
  type        = "string"
}

variable "cluster_master_auth" {
  description = "Data for master cluster authorization"
  type        = "map"
}

variable "jenkins_admin_password" {
  description = "Default password for Jenkins"
  type        = "string"
}

########
# Bucket variables
########

variable "terraform_backend_bucket" {
  description = "Bucket data"

  default = {
    name       = "terraform-backend-my-tech"
    prefix     = "terraform/state"
    location   = "EU"
    versioning = true
  }
}

variable "terraform_backend_bucket_versioning" {
  description = "Bucket versioning status"

  default = {
    enabled = true
  }
}

########
# Google Cloud provider main data
########

variable "GC_provider" {
  description = "Google Cloud provider data"

  default = {
    project = "regal-primacy-196620"
    region  = "europe-west1"
  }
}

########
# Main cluster data
########

variable "cluster_additional_zones" {
  description = "Cluster additional zones"

  default = [
    "europe-west1-d",
    "europe-west1-c",
  ]

  # "europe-west1-b" => Add this on non free-tier account for redundancy
}

variable "cluster_data_basic" {
  description = "Basic cluster data"

  default = {
    name                           = "gc-test-cluster"
    zone                           = "europe-west1-b"
    region                         = "europe-west1"
    remove_default_node_pool       = true
    http_load_balancing_off        = false
    horizontal_pod_autoscaling_off = false
    kubernetes_dashboard_off       = false
  }
}

variable "cluster_nodes" {
  description = "Cluster nodes data"

  default = {
    node_count = 1
    abac       = true
  }
}

variable "cluster_oauth_scopes" {
  description = "Default cluster oauth scopes"

  default = [
    "https://www.googleapis.com/auth/compute",
    "https://www.googleapis.com/auth/devstorage.read_only",
    "https://www.googleapis.com/auth/logging.write",
    "https://www.googleapis.com/auth/monitoring",
  ]
}

variable "cluster_min_master_version" {
  description = "Minimun kubernetes ver. for master"
  default     = "1.11.6-gke.2"
}

variable "cluster_node_pool" {
  description = "Node pool settings"

  default = {
    name                 = "hello-world-cluster-pool"
    region               = "europe-west1"
    initial_node_count   = 1
    autoscaling_max_node = 10
    autoscaling_min_node = 1
    auto_repair          = true
    auto_upgrade         = false
    disk_size_gb         = 10
    machine_type         = "n1-standard-2"
    disk_type            = "pd-standard"
  }
}

variable "cluster_node_pool_oauth" {
  description = "Default oauth scopes for node pool"

  default = [
    "https://www.googleapis.com/auth/compute",
    "https://www.googleapis.com/auth/devstorage.full_control",
    "https://www.googleapis.com/auth/logging.write",
    "https://www.googleapis.com/auth/monitoring",
    "https://www.googleapis.com/auth/ndev.clouddns.readwrite",
  ]
}

########
# Ingress Chart Block
########

variable "ingress-chart" {
  description = "Ingress chart data"

  default = {
    name               = "nginx-ingress"
    chart              = "stable/nginx-ingress"
    namespace          = "nginx-ingress"
    replicaCount       = 2
    updateStrategyType = "RollingUpdate"
    rbac               = false
  }
}

########
# Jenkins Chart Block
########

variable "jenkins-chart" {
  description = "Jenkins chart data"

  default = {
    name      = "jenkins"
    chart     = "stable/jenkins"
    namespace = "jenkins"
  }
}

########
# Namespaces Block
########

variable "prod-namespace" {
  description = "Namespace prod data"

  default = {
    name = "prod"
  }
}
